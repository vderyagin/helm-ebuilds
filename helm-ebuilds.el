;;; helm-ebuilds.el --- Quickly select ebuilds -*- lexical-binding: t -*-

;; Copyright (C) 2015-2021 Victor Deryagin

;; Author: Victor Deryagin <vderyagin@gmail.com>
;; Maintainer: Victor Deryagin <vderyagin@gmail.com>
;; Created: 10 Aug 2015
;; Version: 0.4.1

;; Package-Requires: ((helm))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(require 'ansi-color)
(require 'helm)
(require 'seq)
(require 'subr-x)

(defvar helm-ebuilds-show-only-installed nil)

(defun helm-ebuilds-ensure-executable-exists (executable pkg)
  "Raise error unless EXECUTABLE from PKG is available."
  (unless (executable-find executable)
    (error "Your system does not seem to have '%s' available, merge '%s' package"
           executable pkg)))

(defclass helm-ebuilds-overlay ()
  ((name :initarg :name
         :type string)
   (path :initarg :path
         :type string)))

(defun helm-ebuilds-installed-overlays ()
  "Return list of all installed overlays."
  (let ((regex (rx string-start
                   "[" (+ digit) "]"
                   " \""
                   (group (+ (not (any "\""))))
                   "\" "
                   (group (+ not-newline))
                   string-end)))
    (thread-last (split-string
                  (shell-command-to-string "OVERLAYS_LIST=all PRINT_COUNT_ALWAYS=never eix --not --nocolor")
                  "\n"
                  'omit-empty)
      (seq-filter (lambda (line) (string-match-p regex line)))
      (seq-map
       (lambda (item)
         (string-match regex item)
         (helm-ebuilds-overlay :name (match-string 1 item)
                               :path (file-name-as-directory (match-string 2 item))))))))

(cl-defmethod helm-ebuilds-overlay-packages-process ((overlay helm-ebuilds-overlay))
  (let ((overlay-name (oref overlay name)))
    (start-process (format "eix list packages in overlay %s" overlay-name)
                   nil
                   "eix"
                   "--format"
                   "<installed> <category>/<name> <description>\n"
                   "--pure-packages"
                   "--nocolor"
                   "--in-overlay"
                   overlay-name
                   helm-pattern)))

(defclass helm-ebuilds-package ()
  ((installed :initarg :installed
              :type boolean)
   (name :initarg :name
         :type string)
   (category :initarg :category
             :type string)
   (description :initarg :description
                :type string)))

(defun helm-ebuilds-pkg-line-to-package (pkg-line)
  (let ((pkg-line-re (rx string-start
                         (group (optional "1"))
                         " "
                         (group (+ (not (any "/"))))
                         "/"
                         (group (+ (not (any " "))))
                         " "
                         (group (+ not-newline))
                         string-end)))
    (string-match pkg-line-re pkg-line)
    (helm-ebuilds-package
     :installed   (not (string-empty-p (match-string 1 pkg-line)))
     :category    (match-string 2 pkg-line)
     :name        (match-string 3 pkg-line)
     :description (match-string 4 pkg-line))))

(cl-defmethod helm-ebuilds-package-category+name ((pkg helm-ebuilds-package))
  (with-slots (category name) pkg
    (concat category "/" name)))

(cl-defmethod helm-ebuilds-package-homepage ((pkg helm-ebuilds-package))
  (shell-command-to-string
   (format "eix --format '<homepage>' --nocolor --pure-packages --exact %s"
           (helm-ebuilds-package-category+name pkg))))

(cl-defmethod helm-ebuilds-package-show ((pkg helm-ebuilds-package))
  (with-slots (installed description) pkg
    (let ((category+name (helm-ebuilds-package-category+name pkg)))
      (when installed
        (setq category+name (propertize category+name 'face 'bold)))
      (format "%-60s %s" category+name description))))

(cl-defmethod helm-ebuilds-package-show-use-flags ((pkg helm-ebuilds-package))
  "Show a buffer with listing of PKG's USE flags and their descriptions."
  (helm-ebuilds-ensure-executable-exists "equery" "app-portage/gentoolkit")
  (helm-ebuilds-show-cmd-output
   (format "equery uses -i %s"
           (helm-ebuilds-package-category+name pkg))))

(cl-defmethod helm-ebuilds-package-show-installed-files ((pkg helm-ebuilds-package))
  "Show a buffer with a list of files installed by PKG."
  (let ((category+name (helm-ebuilds-package-category+name pkg)))
    (unless (oref pkg installed)
      (error "Package %s is not installed" category+name))
    (helm-ebuilds-ensure-executable-exists "qlist" "app-portage/portage-utils")
    (helm-ebuilds-show-cmd-output
     (format "qlist --quiet --exact %s" category+name))))

(cl-defmethod helm-ebuilds-package-show-merge-history ((pkg helm-ebuilds-package))
  "Show a buffer with PKG merge history."
  (helm-ebuilds-ensure-executable-exists "genlop" "app-portage/genlop")
  (helm-ebuilds-show-cmd-output
   (format "genlop --time %s" (helm-ebuilds-package-category+name pkg))))

(cl-defmethod helm-ebuilds-package-show-dependents ((pkg helm-ebuilds-package))
  "Show a buffer with list of packages dependent on PKG."
  (helm-ebuilds-ensure-executable-exists "qdepends" "app-portage/portage-utils")
  (helm-ebuilds-show-cmd-output
   (format "qdepends --query %s"
           (helm-ebuilds-package-category+name pkg))))

(defun helm-ebuilds-toggle-show-only-installed ()
  "Toggle showing only installed packages / all packages."
  (interactive)
  (setq helm-ebuilds-show-only-installed (not helm-ebuilds-show-only-installed))
  (helm-force-update))

(define-minor-mode helm-ebuilds-cmd-output-mode
  "Minor mode for displaying shell command output"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "q") #'quit-window)
            (define-key map (kbd "p") #'previous-line)
            (define-key map (kbd "n") #'next-line)
            (define-key map (kbd "<up>") #'previous-line)
            (define-key map (kbd "<down>") #'next-line)
            map)
  (read-only-mode -1)
  (ansi-color-apply-on-region (point-min) (point-max))
  (read-only-mode 1)
  (set-buffer-modified-p nil))

(defun helm-ebuilds-show-cmd-output (cmd)
  "Run CMD and show its output in separate buffer."
  (let ((buf (get-buffer-create " *command output: helm-ebuilds*")))
    (with-current-buffer buf
      (read-only-mode -1)
      (erase-buffer)
      (eshell-command cmd 'insert)
      (helm-ebuilds-cmd-output-mode))
    (pop-to-buffer buf)))

(defclass helm-ebuilds-overlays-source (helm-source)
  ((candidates
    :initform
    (lambda () (object-assoc-list 'name (helm-ebuilds-installed-overlays))))
   (action
    :initform
    '(("Choose package" . (lambda (_)
                            (helm-ebuilds-select-package
                             (helm-marked-candidates))))
      ("Go to root" . (lambda (overlay) (find-file (oref overlay path))))))
   (persistent-action
    :initform
    (lambda (overlay) (find-file (oref overlay path))))
   (persistent-help
    :initform
    "Show root")
   (mode-line
    :initform
    '("overlays" "RET:Select package from overlay(s) f2:Go to root"))))

(defvar helm-ebuilds-select-package-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map helm-map)
    (define-key map (kbd "M-I") #'helm-ebuilds-toggle-show-only-installed)
    map))

(defclass helm-ebuilds-repo-source (helm-source-async)
  ((overlay
    :initarg :overlay
    :type helm-ebuilds-overlay)
   (keymap
    :initform
    'helm-ebuilds-select-package-map)
   (header-name
    :initform
    (lambda (_)
      (let ((overlay (assoc-default 'overlay (helm-get-current-source))))
        (with-slots (name path) overlay
          (format "%s (%s)" name path)))))
   (candidates-process
    :initform
    (lambda ()
      (let ((overlay (assoc-default 'overlay (helm-get-current-source))))
        (helm-ebuilds-overlay-packages-process overlay))))
   (candidate-transformer
    :initform
    (list (apply-partially #'seq-map #'helm-ebuilds-pkg-line-to-package)
          (apply-partially #'seq-filter
                           (lambda (pkg)
                             (or (not helm-ebuilds-show-only-installed)
                                 (oref pkg installed))))
          (apply-partially #'seq-map
                           (lambda (pkg) (cons (helm-ebuilds-package-show pkg) pkg)))))
   (requires-pattern
    :initform
    2)
   (action
    :initform
    '(("Select package file" . (lambda (pkg)
                                 (let ((root (oref (assoc-default 'overlay (helm-get-current-source)) path)))
                                   (helm-ebuilds-select-file
                                    (expand-file-name (helm-ebuilds-package-category+name pkg)
                                                      root)))))
      ("Copy to kill-ring" . (lambda (_)
                               (kill-new
                                (mapconcat #'identity
                                           (seq-map #'helm-ebuilds-package-category+name
                                                    (helm-marked-candidates))
                                           " "))))
      ("Visit homepage" . (lambda (_)
                            (thread-last (helm-marked-candidates)
                              (seq-map #'helm-ebuilds-package-homepage)
                              (seq-each #'browse-url))))
      ("Show USE flags" . helm-ebuilds-package-show-use-flags)
      ("List files" . helm-ebuilds-package-show-installed-files)
      ("Show merge history" . helm-ebuilds-package-show-merge-history)
      ("Show list of dependent packages" . helm-ebuilds-package-show-dependents)))
   (action-transformer
    :initform
    (lambda (actions pkg)
      (seq-filter
       (pcase-lambda (`(,name . _))
         (or (oref pkg installed)
             (not (string= name "List files"))))
       actions)))
   (persistent-action
    :initform
    (lambda (pkg)
      (helm-ebuilds-show-cmd-output
       (format "eix --force-color --exact %s"
               (helm-ebuilds-package-category+name pkg)))))
   (persistent-help
    :initform
    "Show package info")
   (mode-line
    :initform
    (lambda ()
      (list "packages"
            (mapconcat
             #'identity
             (list "RET:Files"
                   "f2:Copy"
                   "f3:Visit homepage"
                   "f4:Show USE"
                   "f5:Installed files"
                   "f6:Merge hist"
                   "f7:Dependents"
                   (format "M-I:Show %s packages"
                           (if helm-ebuilds-show-only-installed
                               "all"
                             "only installed")))
             " "))))))

(defclass helm-ebuilds-package-versions-source (helm-source)
  ((root
    :initarg :root
    :type string)
   (candidates
    :initform
    (lambda ()
      (let ((root (assoc-default 'root (helm-get-current-source))))
        (file-expand-wildcards (expand-file-name "*.ebuild" root)))))
   (real-to-display
    :initform
    'file-name-base)
   (action
    :initform
    '(("Open file" . find-file)))))

(defclass helm-ebuilds-patches-source (helm-source)
  ((root
    :initarg :root
    :type string)
   (candidates
    :initform
    (lambda ()
      (let* ((root (assoc-default 'root (helm-get-current-source)))
             (files-dir (expand-file-name "files" root)))
        (when (file-directory-p files-dir)
          (directory-files-recursively files-dir ".\.patch\\'")))))
   (real-to-display
    :initform
    (lambda (file)
      (let ((root (assoc-default 'root (helm-get-current-source))))
        (string-remove-suffix
         ".patch"
         (file-relative-name file (expand-file-name "files" root))))))
   (action
    :initform
    '(("Open file" . find-file)))))

(defclass helm-ebuilds-other-files-source (helm-source)
  ((root
    :initarg :root
    :type string)
   (candidates
    :initform
    (lambda ()
      (let ((root (assoc-default 'root (helm-get-current-source))))
        (seq-filter
         (lambda (file) (not (string-match-p ".\\.\\(?:ebuild\\|patch\\)\\'" file)))
         (directory-files-recursively root "")))))
   (real-to-display
    :initform
    (lambda (file)
      (let ((root (assoc-default 'root (helm-get-current-source))))
        (file-relative-name file root))))
   (action
    :initform
    '(("Open file" . find-file)))))

(defun helm-ebuilds-select-file (pkg-dir)
  "Preconfigured helm for choosing a file within PKG-DIR."
  (helm :prompt "Ebuild: "
        :buffer "*helm ebuilds: select file*"
        :sources (list
                  (helm-make-source "Versions" 'helm-ebuilds-package-versions-source
                    :root pkg-dir)
                  (helm-make-source "Patches" 'helm-ebuilds-patches-source
                    :root pkg-dir)
                  (helm-make-source "Other files" 'helm-ebuilds-other-files-source
                    :root pkg-dir))))

(defun helm-ebuilds-select-package (&optional overlays)
  (unless overlays
    (setq overlays (helm-ebuilds-installed-overlays)))
  (helm :prompt "Package: "
        :buffer " *helm ebuilds: select package*"
        :sources (seq-map (lambda (overlay)
                            (let ((root (oref overlay path)))
                              (helm-make-source root #'helm-ebuilds-repo-source
                                :overlay overlay)))
                          overlays)))

(defun helm-ebuilds-select-overlay ()
  (helm :prompt "Overlay: "
        :buffer " *helm ebuilds: select overlay*"
        :sources (helm-make-source "Overlays" 'helm-ebuilds-overlays-source)))

;;;###autoload
(defun helm-ebuilds (select-overlay)
  "Preconfigured helm for selecting gentoo packages.
Allows to limit overlays when called with prefix arg."
  (interactive "P")
  (helm-ebuilds-ensure-executable-exists "eix" "app-portage/eix")
  (setq helm-ebuilds-show-only-installed nil)
  (if select-overlay
      (helm-ebuilds-select-overlay)
    (helm-ebuilds-select-package)))

(provide 'helm-ebuilds)

;;; helm-ebuilds.el ends here
